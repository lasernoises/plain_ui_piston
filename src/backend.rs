use piston::window::*;
use piston::input::*;
use piston::event_loop::*;
use graphics::*;
use opengl_graphics::GlGraphics;

use renderer::*;

pub enum PistonEvent<'a> {
    Input(Input, &'a mut PistonTheme, [f32; 2]),
    Render(&'a mut GlGraphics, Viewport, &'a mut PistonTheme, [f32; 2]),
    Update(f64)
}

pub struct PistonBackend<W: Window + BuildFromWindowSettings> {
    pub window: W,
    pub g: GlGraphics,
    pub events: Events,
    pub size: [f32; 2],
    pub theme: PistonTheme
}

impl<W: Window + BuildFromWindowSettings> PistonBackend<W> {
    pub fn next<'a>(&'a mut self) -> Option<PistonEvent<'a>> {
        while let Some(e) = self.events.next(&mut self.window) {
            match e {
                Event::Input(input) => {
                    return Some(PistonEvent::Input(input, &mut self.theme, self.size));
                },
                Event::Loop(loop_event) => {
                    match loop_event {
                        Loop::Render(args) => {
                            self.size = [args.width as f32, args.height as f32];
                            return Some(PistonEvent::Render(
                                &mut self.g,
                                args.viewport(),
                                &mut self.theme,
                                self.size
                            ));
                        }
                        Loop::Update(UpdateArgs { dt }) => {
                            return Some(PistonEvent::Update(dt));
                        }
                        _ => ()
                    }
                }
                _ => ()
            }
        }
        None
    }
}
