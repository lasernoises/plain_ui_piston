use renderer::*;

#[test]
fn rect_poly_inflate_test() {
    {
        let polygon = vec![[0.0; 2], [2.0; 2]];
        assert_eq!(rect_poly_inflate(&polygon, 1.0), vec![[-1.0; 2], [3.0; 2]]);
    }
    {
        let polygon = vec![[0.0; 2], [4.0, 2.0], [8.0, 4.0]];
        assert_eq!(rect_poly_inflate(&polygon, 1.0), vec![[-1.0; 2], [5.0, 1.0], [9.0, 5.0]]);
    }
    {
        let polygon = vec![[0.0, 2.0], [2.0, 0.0], [4.0; 2]];
        assert_eq!(rect_poly_inflate(&polygon, 1.0), vec![[-1.0, 1.0], [1.0, -1.0], [5.0, 5.0]]);
    }
}
