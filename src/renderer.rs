use std::cmp::min;

use plain_ui::*;

use graphics::*;
use graphics::character::CharacterCache;
use opengl_graphics::GlGraphics;
use opengl_graphics::GlyphCache;

pub struct PistonTheme {
    pub border_size: f64,
    pub rect_colors: [[f32; 4]; 2],
    pub font_colors: [[f32; 4]; 2],
    pub font: GlyphCache<'static>
}

impl PistonTheme {
    pub fn new(font: GlyphCache<'static>) -> Self {
        PistonTheme {
            border_size: 16.0,
            rect_colors: [[0.0, 0.0, 0.0, 1.0], [0.5, 0.5, 0.5, 1.0]],
            font_colors: [[1.0, 1.0, 1.0, 1.0], [0.0, 0.0, 0.0, 1.0]],
            font,
        }
    }
}

impl<'a> Theme for PistonTheme {
    fn border_size(&mut self, _: (u8, u8)) -> f32 {
        self.border_size as f32
    }

    fn text_min_size(&mut self, _: (u8, u8), text: &str) -> [f32; 2] {
        let mut x = 0.0;
        let mut y = 0.0;
        for ch in text.chars() {
            if let Ok(character) = self.font.character(32, ch) {
                x += character.width();
                let height = character.texture.get_height() as f64;
                if height > y {
                    y = height;
                }
            }
        }
        [x as f32, y as f32]
    }
}

pub struct PistonRenderer<'a> {
    pub g: &'a mut GlGraphics,
    pub c: &'a Context
}

impl<'a> Renderer for PistonRenderer<'a> {
    type T = PistonTheme;

    fn clear(&mut self, t: &mut PistonTheme, style: (u8, u8)) {
        clear(t.rect_colors[min(style.1 as usize, 1)], self.g);
    }

    fn rect(&mut self, t: &mut PistonTheme, rect: Rect, style: (u8, u8)) {
        /*let mut draw_poly: Vec<[f64; 2]> = Vec::new();

        if polygon.len() > 0 {
            let inflated_poly = rect_poly_inflate(polygon, 16.0);

            let start = [inflated_poly[0][0] as f64, inflated_poly[0][1] as f64];
            draw_poly.push(start);

            let mut last_y = start[1];
            for ref point in &inflated_poly[1..] {
                draw_poly.push([point[0] as f64, last_y]);
                draw_poly.push([point[0] as f64, point[1] as f64]);
                last_y = point[1] as f64;
            }

            draw_poly.push([start[0], last_y]);
        }

        Polygon::new(POLYGON_COLORS[min(style.1 as usize, POLYGON_COLORS.len() - 1)]).draw(
            &draw_poly,
            &Default::default(),
            self.c.transform,
            self.g
        );

        Rectangle::new_border(
            POLYGON_COLORS[min(style.1 as usize, POLYGON_COLORS.len() - 1)],
            16.0
        ).draw(
            [rect.0[0], rect.0[1], rect.1[0], rect.1[1]],
            &Default::default(),
            self.c.transform,
            self.g
        );*/

        rectangle(
            t.rect_colors[min(style.1 as usize, 1)],
            [rect.0[0] as f64, rect.0[1] as f64, rect.1[0] as f64, rect.1[1] as f64],
            self.c.transform,
            self.g
        )
    }

    fn text(&mut self, t: &mut PistonTheme, rect: Rect, style: (u8, u8), font: (u8, u8), text: &str)
    {
        let min_size = t.text_min_size(font, text);
        let image = Image::new_color(t.font_colors[min(style.1 as usize, 1)]);
        let mut x = rect.0[0] as f64;
        let mut y = (rect.0[1] + min_size[1]) as f64;
        for ch in text.chars() {
            if let Ok(character) = t.font.character(32, ch) {
                let ch_x = x + character.left();
                let ch_y = y - character.top();
                image.draw(
                    character.texture,
                    &self.c.draw_state,
                    self.c.transform.trans(ch_x, ch_y),
                    self.g
                );
                x += character.width();
                y += character.height();
            }
        }
    }
}

/*pub fn rect_poly_inflate(polygon: &[[f32; 2]], inflate: f32) -> Vec<[f32; 2]> {
    let mut exp_poly = Vec::new();
    exp_poly.extend_from_slice(polygon);

    for i in 0..exp_poly.len() {
        let j = if i + 1 < exp_poly.len() { i + 1 } else { 0 };

        let diff = [exp_poly[j][0] - exp_poly[i][0], exp_poly[j][1] - exp_poly[i][1]];

        if diff[0] > 0.0 {
            exp_poly[i][1] -= inflate;
        } else {
            exp_poly[i][1] += inflate;
        }

        if diff[1] > 0.0 {
            exp_poly[j][0] += inflate;
        } else {
            exp_poly[j][0] -= inflate;
        }
    }

    exp_poly
}*/
